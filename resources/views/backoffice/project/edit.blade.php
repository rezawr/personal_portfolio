@extends('backoffice.project.create')

@section('overview__value')<a href="{{ $project->file_download }}" target="_blank">Unduh</a>@endsection
@section('name__value'){{ $project->name }}@endsection
@section('client__value'){{ $project->client }}@endsection
@section('delivered__value'){{ $project->delivered }}@endsection
@section('link__value'){{ $project->link }}@endsection
@section('details__value'){{ $project->details }}@endsection

@section('page_js')
@parent
<script>
    $(document).ready(function() {
        let value = "{{ $project->type }}"
        arr_val = value.split(",")

        $(".js-example-tags").select2({
            tags: true,
            multiple: true,
            data: arr_val
        })

        $('.js-example-tags').val(arr_val).trigger("change")
    })

    url = "{{ route('backoffice.project.update', $project->id) }}"
    formDataMethod = "PUT"
</script>
@endsection