@extends('backoffice.project.base')

@section('main-content')
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12">
        <form action="" id="project_form">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Pendidikan</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Nama Project</label>
                        <input type="text" class="form-control" id="name" value="@yield('name__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Gambar</label> <small>@yield('overview__value')</small>
                        <input type="file" class="form-control" id="overview">
                    </div>

                    <div class="form-group">
                        <label for="">Klien</label>
                        <input type="text" class="form-control" id="client" value="@yield('client__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Tanggal</label>
                        <input type="date" class="form-control" id="delivered" value="@yield('delivered__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Link</label>
                        <input type="text" class="form-control" id="link" value="@yield('link__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Jenis Pekerjaan</label>
                        <select name="type" id="type" class="form-control js-example-tags"></select>
                    </div>

                    <div class="form-group">
                        <label for="description">Detail</label>
                        <textarea id="ckeditor" class="form-control">@yield('details__value')</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('backoffice.project.index') }}" class="btn btn-secodary">Kembali</a>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('page_js')
<script>
    CKEDITOR.replace('ckeditor');
    $(document).ready(function() {
        $(".js-example-tags").select2({
            tags: true,
            multiple: true
        })
    })
</script>

<script>
    form = "#project_form"
    url = "{{ route('backoffice.project.store') }}"
    method = "POST"
    formDataMethod = null
    contentType = false
    processData = false

    function generate_payload()
    {
        var formdata = new FormData();

        if (formDataMethod !== null) {
            formdata.append('_method', formDataMethod)
        }

        if (document.getElementById("overview").files[0] !== undefined) {
            formdata.append('overview', document.getElementById("overview").files[0])
        }

        formdata.append("name", $('#name').val())
        formdata.append("client", $('#client').val())
        formdata.append("delivered", $('#delivered').val())
        formdata.append("link", $('#link').val())
        formdata.append("type", $('#type').val())
        formdata.append("details", CKEDITOR.instances.ckeditor.getData())

        return formdata
    }

    function success_callback(data) {
        setTimeout(function () {
            window.location.href = data.data
        }, 500)
    }

</script>
@endsection