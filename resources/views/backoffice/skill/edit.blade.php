@extends('backoffice.skill.create')

@section('name__value'){{ $skill->name }}@endsection

@if($skill->level == 'basic')
    @section('basic__value'){{"selected"}}@endsection
@elseif($skill->level == 'experienced')
    @section('experienced__value'){{"selected"}}@endsection
@elseif($skill->level == 'proficient')
    @section('proficient__value'){{"selected"}}@endsection
@elseif($skill->level == 'extensive')
    @section('extensive__value'){{"selected"}}@endsection
@elseif($skill->level == 'excellent')
    @section('excellent__value'){{"selected"}}@endsection
@endif

@section('page_js')
@parent
<script>
    url = "{{ route('backoffice.skill.update', $skill->id) }}"
    method = "PUT"
</script>
@endsection