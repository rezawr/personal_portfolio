@extends('backoffice.skill.base')

@section('main-content')
<div class="row">
    <div class="col-6">
        <form action="" method="post" id="skill_form">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Skill</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Nama Skill</label>
                        <input type="text" class="form-control" id="name" value="@yield('name__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Level</label>
                        <select name="level" id="level" class="form-control">
                            <option value=""></option>
                            <option @yield('basic__value') value="basic">Basic</option>
                            <option @yield('experienced__value') value="experienced">Experienced</option>
                            <option @yield('proficient__value') value="proficient">Proficient</option>
                            <option @yield('extensive__value') value="extensive">Extensive</option>
                            <option @yield('excellent__value') value="excellent">Excellent</option>
                        </select>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('backoffice.skill.index') }}" class="btn btn-secondary">Back</a>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('page_js')
<script>
    form = "#skill_form"
    url = "{{ route('backoffice.skill.store') }}"
    method = "POST"

    function generate_payload()
    {
        return {
            'name': $('#name').val(),
            'level': $('#level').val()
        }
    }

    function success_callback(data) {
        setTimeout(function () {
            window.location.href = data.data
        }, 500)
    }
</script>
@endsection