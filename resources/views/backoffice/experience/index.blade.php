@extends('backoffice.experience.base')

@section('page_css')
<style>
    table tbody tr:hover {
        cursor: pointer;
        background: #808080
    }

    .come-from-modal.left .modal-dialog,
    .come-from-modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 320px;
        height: 100%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .come-from-modal.left .modal-content,
    .come-from-modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
        border-radius: 0px;
    }

    .come-from-modal.left .modal-body,
    .come-from-modal.right .modal-body {
        padding: 15px 15px 80px;
    }
    .come-from-modal.right.fade .modal-dialog {
        right: -11px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .come-from-modal.right.fade.in .modal-dialog {
        right: 0;
    }
</style>

@endsection
@section('main-content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Informasi Dasar</h4>
                <div class="card-header-action">
                    <a href="{{ route('backoffice.experience.create') }}" class="btn btn-primary">Tambah Pengalaman</a>
                </div>
            </div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Posisi</th>
                            <th>Perusahaan</th>
                            <th>Mulai</th>
                            <th>Berakhir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $row)
                        <tr onclick="show('{{ $row->dependent_url->show }}')">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row->position }}</td>
                            <td>{{ $row->company }}</td>
                            <td>{{ date('F Y', strtotime($row->start)) }}</td>
                            <td>{{ empty($row->end) ? 'current' : date('F Y', strtotime($row->end)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade  come-from-modal right" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <th>Posisi</th>
                        <td id="position__table"></td>
                    </tr>
                    <tr>
                        <th>Institusi/Perusahaan</th>
                        <td id="company__table"></td>
                    </tr>
                    <tr>
                        <th>Logo Institusi/Perusahaan</th>
                        <td id="company_logo__table"></td>
                    </tr>
                    <tr>
                        <th>Mulai</th>
                        <td id="start__table"></td>
                    </tr>
                    <tr>
                        <th>Selesai</th>
                        <td id="end__table"></td>
                    </tr>
                    <tr>
                        <th>Deskripsi</th>
                        <td id="description__table"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="modal__edit_action" class="btn btn-primary">Edit</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script>
    function show(url) {
        $.ajax({
            url: url,
            method: "GET",
            success: function (data) {
                let start = new Date(data.data.start);
                startDate = (start.getMonth() + 1) + "/" + start.getFullYear()

                endDate = "Current"
                if (data.data.end) {
                    let end = new Date(data.data.end);
                    endDate = (end.getMonth() + 1) + "/" + end.getFullYear()
                }

                $('#position__table').html(data.data.position)
                $('#company__table').html(data.data.company)
                $('#company_logo__table').html(`<a href=`+ data.data.file_download +` target='_blank'>Unduh</a>`)
                $('#start__table').html(startDate)
                $('#end__table').html(endDate)
                $('#description__table').html(data.data.description)

                $('#modal__edit_action').click(function (e) {
                    e.preventDefault()

                    window.location.href = data.data.dependent_url.edit
                })
                $('#myModal').modal('show')
            },
            error: function(xhr) {
                if (typeof xhr.responseJSON.message === "string") {
                    message = xhr.responseJSON.message
                } else {
                    message = xhr.responseJSON.message.join()
                }

                event.preventDefault();
                iziToast.error({
                    title: xhr.statusText,
                    message: message,
                    position: 'topRight'
                });
            }
        })
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
</script>
@endsection
