@extends('backoffice.experience.create')

@section('position__value'){{ $experience->position }}@endsection
@section('company__value'){{ $experience->company }}@endsection
@section('logo__value')<a href="{{ $experience->file_download }}" target="_blank">Unduh</a>@endsection
@section('start__value'){{ date('Y-m', strtotime($experience->start)) }}@endsection
@section('end__value'){{ date('Y-m', strtotime($experience->end)) }}@endsection
@section('description__value'){{ $experience->description }}@endsection

@section('page_js')
@parent
<script>
    let check = {{ $experience->current }}
    checkbox.checked = check

    if (check) {
        $('#end').attr('hidden', 'hidden');
    } else {
        $('#end').removeAttr('hidden', 'hidden');
    }

    url = "{{ route('backoffice.experience.update', $experience->id) }}"
    formDataMethod = "PUT"
</script>
@endsection
