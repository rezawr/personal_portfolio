@extends('backoffice.experience.base')

@section('main-content')
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12">
        <form action="" id="experience_form">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Pendidikan</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Posisi</label>
                        <input type="text" class="form-control" id="position" value="@yield('position__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Insitusi/Perusahaan</label>
                        <input type="text" class="form-control" id="company" value="@yield('company__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Logo Perusahaan</label>
                        <small>@yield('logo__value')</small>
                        <input type="file" name="logo" id="logo" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="">Mulai</label>
                        <input type="month" class="form-control" id="start" value="@yield('start__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Selesai</label>
                        <input type="month" class="form-control" id="end" value="@yield('end__value')">
                        <input type="checkbox" name="current" id="current" value="true" @yield('current__value')> <label for="">Current</label>
                    </div>

                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea id="ckeditor" class="form-control">@yield('description__value')</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('backoffice.experience.index') }}" class="btn btn-secodary">Kembali</a>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('page_js')
<script>
    CKEDITOR.replace('ckeditor');
</script>
<script>
    form = "#experience_form"
    url = "{{ route('backoffice.experience.store') }}"
    method = "POST"
    formDataMethod = null
    contentType = false
    processData = false

    function generate_payload()
    {
        var formdata = new FormData();

        if (formDataMethod !== null) {
            formdata.append('_method', formDataMethod)
        }

        if (document.getElementById("logo").files[0] !== undefined) {
            formdata.append('logo', document.getElementById("logo").files[0])
        }

        formdata.append("position", $('#position').val())
        formdata.append("company", $('#company').val())
        formdata.append("start", $('#start').val())
        formdata.append("description", CKEDITOR.instances.ckeditor.getData())

        if (checkbox.checked) {
            formdata.append("current", 1)
        } else {
            formdata.append("end", $('#end').val())
            formdata.append("current", 0)
        }

        return formdata
    }

    function success_callback(data) {
        setTimeout(function () {
            window.location.href = data.data
        }, 500)
    }

    let checkbox = document.getElementById("current");
    checkbox.addEventListener( "change", () => {
        if ( checkbox.checked ) {
            $('#end').attr('hidden', 'hidden');
        } else {
            $('#end').removeAttr('hidden', 'hidden');
        }
    });

</script>
@endsection