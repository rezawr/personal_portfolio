@extends('backoffice.education.base')

@section('main-content')
<div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12">
        <form action="" id="education_form">
            <div class="card">
                <div class="card-header">
                    <h4>Tambah Pendidikan</h4>
                </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="">Level</label>
                        <select name="level" id="level" class="form-control">
                            <option @yield('non-formal__value') value="non-formal">Non formal</option>
                            <option @yield('high-school__value') value="high-school">Sekolah Menengah</option>
                            <option @yield('undergraduate__value') value="undergraduate">Sarjana</option>
                            <option @yield('graduate__value') value="graduate">Pasca Sarjana</option>
                            <option @yield('postgraduate__value') value="postgraduate">Doktoral</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Major/Jurusan</label>
                        <input type="text" class="form-control" id="major" value="@yield('major__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Insitusi</label>
                        <input type="text" class="form-control" id="institution" value="@yield('institution__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Mulai</label>
                        <input type="month" class="form-control" id="start" value="@yield('start__value')">
                    </div>

                    <div class="form-group">
                        <label for="">Selesai</label>
                        <input type="month" class="form-control" id="end" value="@yield('end__value')">
                    </div>

                    <div class="form-group">
                        <label for="description">Deskripsi</label>
                        <textarea id="ckeditor" class="form-control">@yield('description__value')</textarea>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="{{ route('backoffice.education.index') }}" class="btn btn-secodary">Kembali</a>
                    <button class="btn btn-primary">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@section('page_js')
<script>
    CKEDITOR.replace('ckeditor');
</script>
<script>
    form = "#education_form"
    url = "{{ route('backoffice.education.store') }}"
    method = "POST"

    function generate_payload()
    {
        return {
            'level': $('#level').val(),
            'major': $('#major').val(),
            'institution': $('#institution').val(),
            'start': $('#start').val(),
            'end': $('#end').val(),
            'description': CKEDITOR.instances.ckeditor.getData(),
        }
    }

    function success_callback(data) {
        setTimeout(function () {
            window.location.href = data.data
        }, 500)
    }

</script>
@endsection