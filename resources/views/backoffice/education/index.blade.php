@extends('backoffice.education.base')

@section('page_css')
<style>
    table tbody tr:hover {
        cursor: pointer;
        background: #808080
    }

    .come-from-modal.left .modal-dialog,
    .come-from-modal.right .modal-dialog {
        position: fixed;
        margin: auto;
        width: 320px;
        height: 100%;
        -webkit-transform: translate3d(0%, 0, 0);
        -ms-transform: translate3d(0%, 0, 0);
        -o-transform: translate3d(0%, 0, 0);
        transform: translate3d(0%, 0, 0);
    }

    .come-from-modal.left .modal-content,
    .come-from-modal.right .modal-content {
        height: 100%;
        overflow-y: auto;
        border-radius: 0px;
    }

    .come-from-modal.left .modal-body,
    .come-from-modal.right .modal-body {
        padding: 15px 15px 80px;
    }
    .come-from-modal.right.fade .modal-dialog {
        right: -11px;
        -webkit-transition: opacity 0.3s linear, right 0.3s ease-out;
        -moz-transition: opacity 0.3s linear, right 0.3s ease-out;
        -o-transition: opacity 0.3s linear, right 0.3s ease-out;
        transition: opacity 0.3s linear, right 0.3s ease-out;
    }

    .come-from-modal.right.fade.in .modal-dialog {
        right: 0;
    }
</style>

@endsection
@section('main-content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Informasi Dasar</h4>
                <div class="card-header-action">
                    <a href="{{ route('backoffice.education.create') }}" class="btn btn-primary">Tambah Pendidikan</a>
                </div>
            </div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Level Pendidikan</th>
                            <th>Jurusan</th>
                            <th>Institusi</th>
                            <th>Mulai</th>
                            <th>Sampai</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $row)
                        <tr onclick="show('{{ $row->dependent_url->show }}')">
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ implode(" ", explode('-', ucfirst($row->level))) }}</td>
                            <td>{{ $row->major }}</td>
                            <td>{{ $row->institution }}</td>
                            <td>{{ date('F Y', strtotime($row->start)) }}</td>
                            <td>{{ date('F Y', strtotime($row->end)) }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade  come-from-modal right" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <th>Level Pendidikan</th>
                        <td id="level__table"></td>
                    </tr>
                    <tr>
                        <th>Institusi</th>
                        <td id="institution__table"></td>
                    </tr>
                    <tr>
                        <th>Jurusan</th>
                        <td id="major__table"></td>
                    </tr>
                    <tr>
                        <th>Mulai</th>
                        <td id="start__table"></td>
                    </tr>
                    <tr>
                        <th>Selesai</th>
                        <td id="end__table"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="modal__edit_action" class="btn btn-primary">Edit</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_js')
<script>
    function show(url) {
        $.ajax({
            url: url,
            method: "GET",
            success: function (data) {
                let start = new Date(data.data.start);
                let end = new Date(data.data.end);
                startDate = (start.getMonth() + 1) + "/" + start.getFullYear()
                endDate = (end.getMonth() + 1) + "/" + end.getFullYear()

                $('#level__table').html(capitalizeFirstLetter(data.data.level))
                $('#institution__table').html(data.data.institution)
                $('#major__table').html(data.data.major)
                $('#start__table').html(startDate)
                $('#end__table').html(endDate)

                $('#modal__edit_action').click(function (e) {
                    e.preventDefault()

                    window.location.href = data.data.dependent_url.edit
                })
                $('#myModal').modal('show')
            },
            error: function(xhr) {
                if (typeof xhr.responseJSON.message === "string") {
                    message = xhr.responseJSON.message
                } else {
                    message = xhr.responseJSON.message.join()
                }

                event.preventDefault();
                iziToast.error({
                    title: xhr.statusText,
                    message: message,
                    position: 'topRight'
                });
            }
        })
    }

    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
</script>
@endsection
