@extends('backoffice.education.create')

@if($education->level == 'non-formal')
    @section('non-formal__value'){{"selected"}}@endsection
@elseif($education->level == 'high-school')
    @section('high-school__value'){{"selected"}}@endsection
@elseif($education->level == 'undergraduate')
    @section('undergraduate__value'){{"selected"}}@endsection
@elseif($education->level == 'graduate')
    @section('graduate__value'){{"selected"}}@endsection
@elseif($education->level == 'postgraduate')
    @section('postgraduate__value'){{"selected"}}@endsection
@endif

@section('major__value'){{ $education->major }}@endsection
@section('institution__value'){{ $education->institution }}@endsection
@section('start__value'){{ date('Y-m', strtotime($education->start)) }}@endsection
@section('end__value'){{ date('Y-m', strtotime($education->end)) }}@endsection
@section('description__value'){{ $education->description }}@endsection

@section('page_js')
@parent
<script>
    url = "{{ route('backoffice.education.update', $education->id) }}"
    method = "PUT"
</script>
@endsection