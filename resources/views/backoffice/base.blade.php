<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <!-- General CSS Files -->
    <link rel='shortcut icon' type='image/x-icon' href="{{ asset('backoffice/assets/img/favicon.ico') }}" />

    <link rel="stylesheet" href="{{ asset('backoffice/assets/css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/bootstrap-social/bootstrap-social.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('backoffice/assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/css/components.css') }}">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('backoffice/assets/css/custom.css') }}">

    <!-- third party CSS -->
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/izitoast/css/iziToast.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/datatables/DataTables-1.10.16/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/jquery-selectric/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backoffice/assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">

    @yield('custom_css')
</head>

<body>
    <div class="loader"></div>
    <div id="app">
        <section class="section">
            @yield('content')
        </section>
    </div>
    <!-- General JS Scripts -->
    <script src="{{ asset('backoffice/assets/js/app.min.js') }}"></script>
    <!-- JS Libraies -->
    <!-- Page Specific JS File -->
    <!-- Template JS File -->
    <script src="{{ asset('backoffice/assets/js/scripts.js') }}"></script>
    <!-- Custom JS File -->
    <script src="{{ asset('backoffice/assets/js/custom.js') }}"></script>

    <!-- third party JS -->
    <script src="{{ asset('backoffice/assets/bundles/izitoast/js/iziToast.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/datatables/DataTables-1.10.16/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/prism/prism.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/cleave-js/dist/cleave.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/cleave-js/dist/addons/cleave-phone.us.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/jquery-pwstrength/jquery.pwstrength.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/js/page/forms-advanced-forms.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/jquery-validation/dist/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('backoffice/assets/bundles/jquery-steps/jquery.steps.min.js') }}"></script>
    <script src="https://cdn.ckeditor.com/4.19.1/standard/ckeditor.js"></script>
    <script src="{{ asset('backoffice/assets/bundles/sweetalert/sweetalert.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('backoffice/assets/js/page/sweetalert.js') }}"></script>
    <script>
        let form, url, method;

        // defined default value for ajax settings for accomodate formdata or general utf-8 format
        let contentType = "application/x-www-form-urlencoded; charset=UTF-8"
        let processData = true

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

    @yield('modal')
    @yield('custom_js')
    <script>
        $(form).submit(function(event) {
            event.preventDefault();

            $.ajax({
                url: url,
                method: method,
                dataType : 'json',
                cache : false,
                contentType : contentType,
                processData : processData,
                data: generate_payload(),
                success: function(data) {
                    event.preventDefault();
                    iziToast.success({
                        title: data.message,
                        message: "",
                        position: 'topRight'
                    });

                    success_callback(data);
                },
                error: function(xhr) {
                    if (typeof xhr.responseJSON.message === "string") {
                        message = xhr.responseJSON.message
                    } else {
                        message = xhr.responseJSON.message.join()
                    }

                    event.preventDefault();
                    iziToast.error({
                        title: xhr.statusText,
                        message: message,
                        position: 'topRight'
                    });

                    fail_callback(xhr);
                }
            })
        })
    </script>
    <!-- Extra JS if in modal -->
</body>


<!-- Mirrored from radixtouch.in/templates/admin/aegis/source/light/auth-login.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Sep 2019 06:54:04 GMT -->

</html>

