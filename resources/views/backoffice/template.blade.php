@extends('backoffice.base')

@section('custom_css')
    @yield('page_css')
@endsection

@section('content')
    <div class="main-wrapper main-wrapper-1">
        <div class="navbar-bg"></div>
        <!-- navbar -->
        <nav class="navbar navbar-expand-lg main-navbar">
            <div class="form-inline mr-auto">
                <ul class="navbar-nav mr-3">
                    <li><a href="#" data-toggle="sidebar"
                            class="nav-link nav-link-lg
                                    collapse-btn"> <i
                                data-feather="align-justify"></i></a></li>
                    <li><a href="#" class="nav-link nav-link-lg fullscreen-btn">
                            <i data-feather="maximize"></i>
                        </a></li>
                    <li>
                        <form class="form-inline mr-auto">
                            <div class="search-element">
                                <input class="form-control" type="search" placeholder="Search" aria-label="Search"
                                    data-width="200">
                                <button class="btn" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                            </div>
                        </form>
                    </li>
                </ul>
            </div>
            <ul class="navbar-nav navbar-right">
                <!-- Message and notification list -->
                <!-- end here -->>

                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user"> <img
                            alt="image" src="{{ asset('backoffice/assets/img/user.png') }}"
                            class="user-img-radious-style"> <span class="d-sm-none d-lg-inline-block"></span></a>
                    <div class="dropdown-menu dropdown-menu-right pullDown">
                        <div class="dropdown-title">Halo, Admin</div>
                        <a href="" class="dropdown-item has-icon">
                            <i class="far fa-user"></i> Profile
                        </a>
                        <a href="timeline.html" class="dropdown-item has-icon">
                            <i class="fas fa-bolt"></i>
                            Activities
                        </a>
                        <a href="#" class="dropdown-item has-icon"> <i class="fas fa-cog"></i>
                            Settings
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger"> <i
                                class="fas fa-sign-out-alt"></i>
                            Logout
                        </a>
                    </div>
                </li>
            </ul>
        </nav>

        <!-- sidebar -->
        <div class="main-sidebar sidebar-style-2">
            <aside id="sidebar-wrapper ">
                <div class="sidebar-brand mb-5 py-3">
                    <a href="{{ route('backoffice.index') }}">
                        <img src="{{asset('backoffice/assets/img/logo.png')}}" class="header-logo">
                        <span class="logo-name">Portfolio</span>
                    </a>
                </div>

                <div class="sidebar-user">
                    <div class="sidebar-user-picture">
                        <img alt="image" src="{{ asset('backoffice/assets/img/userbig.png') }}">
                    </div>
                    <div class="sidebar-user-details">
                        <div class="user-name">Admin</div>
                        <div class="user-role">
                            ADMIN
                        </div>
                    </div>
                </div>
                <ul class="sidebar-menu">
                    <li class="menu-header">Utama</li>
                    <li class="@yield('home__active')">
                        <a href="{{ route('backoffice.index') }}" class="nav-link">
                            <i class="fas fa-briefcase"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>

                    <li class="@yield('information__active')">
                        <a href="{{ route('backoffice.information.index') }}" class="nav-link">
                            <i class="fas fa-book"></i>
                            <span>Infomasi</span>
                        </a>
                    </li>

                    <li class="@yield('education__active')">
                        <a href="{{ route('backoffice.education.index') }}" class="nav-link">
                            <i class="fas fa-user-graduate"></i>
                            <span>Pendidikan</span>
                        </a>
                    </li>

                    <li class="@yield('skill__active')">
                        <a href="{{ route('backoffice.skill.index') }}" class="nav-link">
                            <i class="fas fa-ruler-horizontal"></i>
                            <span>Skill</span>
                        </a>
                    </li>

                    <li class="@yield('project__active')">
                        <a href="{{ route('backoffice.project.index') }}" class="nav-link">
                            <i class="fas fa-suitcase"></i>
                            <span>Project</span>
                        </a>
                    </li>

                    <li class="@yield('experience__active')">
                        <a href="{{ route('backoffice.experience.index') }}" class="nav-link">
                            <i class="fas fa-chalkboard"></i>
                            <span>Pengalaman</span>
                        </a>
                    </li>
                </ul>
            </aside>
        </div>

        <!-- main content -->
        <div class="main-content">
            <section class="section">
                <div class="section-body">
                    <div id="loadericon"></div>
                    @yield('main-content')
                </div>
            </section>
        </div>

        <footer class="main-footer">
            <div class="footer-left">
                Copyright &copy; 2019 <div class="bullet"></div> Design and developed By <a target="_blank"
                    href="https://rezawramadhan.xyz/">BigR</a>
            </div>
            <div class="footer-right">
            </div>
        </footer>
    </div>
@endsection

@section('custom_js')
    @yield('page_js')

    <script>
        function ajax_delete(url) {
            $.ajax({
                url: url,
                method:"DELETE",
                success: function(data) {
                    event.preventDefault();
                    iziToast.success({
                        title: data.message,
                        message: "",
                        position: 'topRight'
                    });

                    success_callback(data);
                },
                error: function(xhr) {
                    if (typeof xhr.responseJSON.message === "string") {
                        message = xhr.responseJSON.message
                    } else {
                        message = xhr.responseJSON.message.join()
                    }

                    event.preventDefault();
                    iziToast.error({
                        title: xhr.statusText,
                        message: message,
                        position: 'topRight'
                    });

                    fail_callback(xhr);
                }
            })
        }
    </script>
@endsection
