@extends('backoffice.information.base')

@section('main-content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Informasi Dasar</h4>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Key</th>
                            <th>Value</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ ucfirst($row->key) }}</td>
                            @if($row->type == "file" && !empty($row->value))
                                <th><a href="{{ $row->value }}">Unduh</a></th>
                            @else
                                <th>{{ $row->value }}</th>
                            @endif
                            <td>
                                <button class="btn btn-icon btn-info" onclick="edit('{{ $row->dependent_url->update }}', '{{ ucfirst($row->key) }}', '{{ $row->value }}', '{{ $row->type }}')">
                                    <i class="far fa-edit"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="editFormModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
        <form action="" id="edit_form">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" id="edit_url" hidden>
                    <div class="form-group">
                        <label for="" id="labelEditForm"></label>
                        <input class="form-control" id="valueEditForm">
                    </div>
                </div>
                <div class="modal-footer bg-whitesmoke br" id="editFormModalFooter">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
	</div>
</div>
@endsection

@section('page_js')
<script>
    function edit(url, title, value, type)
    {
        $('#edit_url').val(url)
        $('#exampleModalLabel').html("Edit " + title)
        $('#labelEditForm').html(title)

        if (type == "string") {
            $('#valueEditForm').attr("type", "text");
        } else {
            $('#valueEditForm').attr("type", "file");
        }

        $('#editFormModal').modal('show');
    }

    $('#edit_form').submit(function (e) {
        e.preventDefault();

        var formdata = new FormData();
        formdata.append('_method', "PUT");

        if (document.getElementById("valueEditForm").files !== null) {
            formdata.append('value', document.getElementById("valueEditForm").files[0])
        } else {
            formdata.append('value', $('#valueEditForm').val())
        }

        $.ajax({
            url: $('#edit_url').val(),
            method:"POST",
            dataType : 'json',
            cache : false,
            contentType : false,
            processData : false,
            data: formdata,
            success: function(data) {
                event.preventDefault();
                iziToast.success({
                    title: data.message,
                    message: "",
                    position: 'topRight'
                });
            },
            error: function(xhr) {
                if (typeof xhr.responseJSON.message === "string") {
                    message = xhr.responseJSON.message
                } else {
                    message = xhr.responseJSON.message.join()
                }

                event.preventDefault();
                iziToast.error({
                    title: xhr.statusText,
                    message: message,
                    position: 'topRight'
                });
            }
        })
    })
</script>
@endsection