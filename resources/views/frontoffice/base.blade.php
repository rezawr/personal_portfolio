<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <link rel="icon" href="{{ asset('frontoffice/assets/images/site/fav-icon.png') }}">

    <!--APPLE TOUCH ICON-->
    <link rel="apple-touch-icon" href="{{ asset('frontoffice/assets/images/site/apple-touch-icon.png') }}">


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:500' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Muli' rel='stylesheet' type='text/css'>


    <!-- MATERIAL ICON FONT -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- FONT AWESOME -->
    <link href="{{ asset('frontoffice/assets/stylesheets/vendors/font-awesome.min.css') }}" rel="stylesheet">


    <!-- ANIMATION -->
    <link href="{{ asset('frontoffice/assets/stylesheets/vendors/animate.min.css') }}" rel="stylesheet">

    <!-- MAGNIFICENT POPUP -->
    <link href="{{ asset('frontoffice/assets/stylesheets/vendors/magnific-popup.css') }}" rel="stylesheet">

    <!-- SWIPER -->
    <link href="{{ asset('frontoffice/assets/stylesheets/vendors/swiper.min.css') }}" rel="stylesheet">


    <!-- MATERIALIZE -->
    <link href="{{ asset('frontoffice/assets/stylesheets/vendors/materialize.css') }}" rel="stylesheet">
    <!-- BOOTSTRAP -->
    <link href="{{ asset('frontoffice/assets/stylesheets/vendors/bootstrap.min.css') }}" rel="stylesheet">

    <!-- CUSTOM STYLE -->
    <link href="{{ asset('frontoffice/assets/stylesheets/style_dark_dark.css') }}" id="switch_style" rel="stylesheet">
    @if (!empty($data->background))
    <style>
        .header-background {
            position: relative;
            display: block;
            width: 100%;
            height: 370px;
            background: url('{{ asset("/storage/basic_information/{$data->background}") }}') no-repeat !important;
            background-size: cover !important;
            background-position: center center;
            background-attachment: fixed;
        }
    </style>
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js?client=ca-pub-8935319373441784" crossorigin="anonymous"></script>
</head>
<body>
    <!--==========================================
                  PRE-LOADER
    ===========================================-->
    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="box-holder animated bounceInDown">
                    <span class="load-box"><span class="box-inner"></span></span>
                </div>
                <!-- NAME & STATUS -->
                <div class="text-holder text-center">
                    <center>
                        <h2>{{ empty($data->name) ? "NAME" : $data->name }}</h2>
                        <h6>{{ empty($data->position) ? "POSITION" : $data->position }}</h6>
                    </center>
                </div>
            </div>
        </div>
    </div>

    <!--==========================================
                    HEADER
    ===========================================-->
    <header id="home">
        @yield('menu')

        <!--HEADER BACKGROUND-->
        <div class="header-background section"></div>

    </header>

    @yield('main-content')

    <!--==========================================
                     SCROLL TO TOP
    ===========================================-->
    <div id="scroll-top">
        <div id="scrollup"><i class="fa fa-angle-up"></i></div>
    </div>

    <!--==========================================
                        FOOTER
    ===========================================-->

    <footer style="margin-top: 2%">
        <div class="container">
            <!--FOOTER DETAILS-->
            <p class="text-center">
                © Material CV. All right reserved by
                <a href="https://rezawramadhan.xyz/" target="_blank">
                    <strong>Reza Wahyu Ramadhan</strong>
                </a>
            </p>
        </div>
    </footer>

    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/jquery-2.1.3.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/materialize.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBI14J_PNWVd-m0gnUBkjmhoQyNyd7nllA"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/markerwithlabel.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/retina.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/swiper.jquery.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/vendors/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('frontoffice/assets/javascript/custom-dark.js') }}"></script>

    @yield('js')
</body>
</html>