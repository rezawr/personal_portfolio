@extends('frontoffice.base')

@section('title') {{ $portfolio->name }} @endsection

@section('main-content')
<!--==========================================
                  BACK TO HOME
===========================================-->
<div class="back-button"><span><i class="fa fa-angle-double-left"></i></span></div>

<section class="single-page">
    <div class="container">
        <!--SECTION TITLE-->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="{{ asset('frontoffice/assets/images/icons/safe.png') }}" alt="demo">Portfolios</h4>
        </div>

        <div class="row" id="primary">
            <div class="col-12" id="content">
                <div class="post card">
                    <div class="post-image">
                        <div id="clients-slider" class="swiper-container swiper-container-portfolios">
                            <div class="swiper-wrapper">
                                <!-- SLIDE ONE -->
                                <div class="swiper-slide">
                                    <img class="img-responsive" alt="demo" src="{{ $portfolio->file_download }}">
                                </div>
                            </div>
                            <!-- ADD PAGINATION -->
                            <div class="swiper-pagination swiper-pagination-portfolios"></div>
                        </div>
                    </div>
                    <!--PORTFOLIO TITLE AND DETAILS-->
                    <header>
                        <h2> {{ $portfolio->name }}
                            <time datetime="2045-08-16">{{ date('F d,Y') }}</time>
                        </h2>
                        <hr>
                        <div class="project-detail">
                            <p><strong>Client :</strong>{{ $portfolio->client }}</p>
                            <p><strong>Delivered :</strong>{{ date('d F,Y', strtotime($portfolio->delivered)) }}</p>
                            <p><strong>Link :</strong><a href="{{ $portfolio->link }}" target="_blank">{{ $portfolio->link }}</a>
                            </p>
                            <p><strong>Type :</strong>{{ $portfolio->type }}</p>
                        </div>
                    </header>

                    <!--BODY-->
                    <div class="post-body">
                        <h3>Project Details</h3>
                        {!! $portfolio->details !!}

                        <h3>Client</h3>
                        <div class="client-info">
                            <p>{{ $portfolio->client }}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $('.back-button').click(function (e) {
        window.location.href = "{{ route('index') }}/#portfolio"
    })
</script>
@endsection