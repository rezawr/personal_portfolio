@extends('frontoffice.base')

@section('title') {{ empty($data->name) ? "Personal Portfolio" : (empty($data->position) ? $data->name : "{$data->name} | {$data->position}") }} @endsection

@section('menu')
<nav id="theMenu" class="menu">

    <!--MENU-->
    <div id="menu-options" class="menu-wrap">

        <!--PERSONAL LOGO-->
        <div class="logo-flat">
            <img alt="personal-logo" class="img-responsive" src="{{ asset('frontoffice/assets/images/profile/john.png') }}">
        </div>
        <br>

        <!--OPTIONS-->
        <a href="#home"><i class="title-icon fa fa-user"></i>Home</a>
        <a href="#about"><i class="title-icon fa fa-dashboard"></i>About</a>
        <a href="#education"><i class="title-icon fa fa-graduation-cap"></i>Education</a>
        <a href="#skills"><i class="title-icon fa fa-sliders"></i>Skills</a>
        <a href="#experience"><i class="title-icon fa fa-suitcase"></i>Experience</a>
        <a href="#portfolios"><i class="title-icon fa fa-archive"></i>Portfolios</a>
        <a href="#blog"><i class="title-icon fa fa-pencil-square"></i>Blog</a>
        <a href="#contact"><i class="title-icon fa fa-envelope"></i>Contact</a>
    </div>

    <!-- MENU BUTTON -->
    <div id="menuToggle">
        <div class="toggle-normal">
            <i class="material-icons top-bar">remove</i>
            <i class="material-icons middle-bar">remove</i>
            <i class="material-icons bottom-bar">remove</i>
        </div>
    </div>
</nav>
@endsection

@section('main-content')
@include('frontoffice.section.card')
@include('frontoffice.section.about')
@include('frontoffice.section.education')
@include('frontoffice.section.skill')
@include('frontoffice.section.experience')
@include('frontoffice.section.portfolio')
@include('frontoffice.section.blog')
@include('frontoffice.section.contact')

<section>
    <!--MODAL ONE-->
    <div class="modal fade" id="educationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-1">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!--MODAL HEADER-->
                <div class="modal-header  text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"><i class="fa fa-close"></i></span></button>
                    <h4 class="modal-title" id="educationTitle"></h4>
                    <h6>
                        <small id="educationPeriod"></small>
                    </h6>
                </div>
                <!--MODAL BODY-->
                <div class="modal-body" id="educationDescription">
                </div>
                <!--MODAL FOOTER-->
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </div>
    </div>

    <!--MODAL FOUR-->
    <div class="modal fade" id="experienceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel-4">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <!--MODAL HEADER-->
                <div class="modal-header  text-center">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true"><i class="fa fa-close"></i></span></button>
                    <h4 class="modal-title" id="experienceModalTitle"></h4>
                    <h6>
                        <small id="experienceModalPeriod"></small>
                    </h6>
                </div>
                <!--MODAL BODY-->
                <div class="modal-body" id="experienceModalDescription">
                </div>
                <!--MODAL FOOTER-->
                <div class="modal-footer">
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function(e) {
        getEducation()
        getSkill()
        getExperience()
        getPortfolio()
    })
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#contact-form-custom').submit(function (e) {
        e.preventDefault()
        $.ajax({
            url: "{{ route('post.message') }}",
            method: "POST",
            data: {
                name: $('#first_name').val(),
                subject: $('#sub').val(),
                email: $('#email').val(),
                message: $('#textarea1').val(),
            },
            success: function (data) {
                event.preventDefault();
                Materialize.toast(data.message, 4000);
            },
            error: function (xhr) {
                if (typeof xhr.responseJSON.message === "string") {
                    message = xhr.responseJSON.message
                } else {
                    message = xhr.responseJSON.message.join()
                }

                event.preventDefault();
                Materialize.toast(message , 4000, 'red darken-4');
            }
        })
    })
</script>
@endsection