<!--==========================================
                   EXPERIENCE
===========================================-->
<section id="experience" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="{{ asset('frontoffice/assets/images/icons/layers.png') }}" alt="demo">Experience</h4>
        </div>

        <div id="timeline-experience"></div>
    </div>
</section>

<script>
    function getExperience()
    {
        $.ajax({
            url: "{{ route('get.experience') }}",
            success: function (data) {
                let result = data.data
                const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

                html = ""
                result.forEach(function (value, index) {
                    let start = new Date(value.start);
                    startDate = (monthNames[start.getMonth()]) + " " + start.getFullYear()

                    endDate = "Current"
                    if (!value.current) {
                        let end = new Date(value.end);
                        endDate = (monthNames[end.getMonth()]) + " " + end.getFullYear()
                    }

                    html += `
                    <!-- FIRST TIMELINE -->
                    <div class="timeline-block">
                        <!-- DOT -->
                        <div class="timeline-dot" style="background: url(`+ value.file_download +`) no-repeat !important; background-size: cover !important;"></div>
                        <!-- TIMELINE CONTENT -->
                        <div class="card timeline-content">
                            <div class="card-content">
                                <!-- TIMELINE TITLE -->
                                <h6 class="timeline-title">`+ value.position +`</h6>
                                <!-- TIMELINE TITLE INFO -->
                                <div class="timeline-info">
                                    <h6>
                                        <small>`+ value.company +`</small>
                                    </h6>
                                    <h6>
                                        <small>`+ startDate +` - `+ endDate +`</small>
                                    </h6>
                                </div>
                                <!-- TIMELINE PARAGRAPH -->
                                <p>
                                    `+ value.short_description +`
                                </p>
                                <!-- BUTTON TRIGGER MODAL -->
                                <a href="#" class="modal-dot" data-toggle="modal" data-target="#myModal-4" onclick="getSingleExperience('`+ value.show +`')">
                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    `
                })

                $('#timeline-experience').html(html)
            },
            error: function (xhr) {
                console.log(xhr)
            }
        })
    }

    function getSingleExperience(url)
    {
        $.ajax({
            url: url,
            success: function (data) {
                const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                let result = data.data;
                let start = new Date(result.start);
                startDate = (monthNames[start.getMonth()]) + " " + start.getFullYear()

                endDate = "Current"
                if (!result.current) {
                    let end = new Date(result.end);
                    endDate = (monthNames[end.getMonth()]) + " " + end.getFullYear()
                }

                $('#experienceModalTitle').html(result.position + " at " + result.company)
                $('#experienceModalPeriod').html(startDate + " - " + endDate)
                $('#experienceModalDescription').html(result.description)
                $('#experienceModal').modal('show');
            },
            error: function (xhr) {
                console.log(xhr)
            }
        })
    }
</script>