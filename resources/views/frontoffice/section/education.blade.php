<!--==========================================
                   EDUCATION
===========================================-->

<section id="education" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="{{ asset('frontoffice/assets/images/icons/book.png') }}" alt="demo">Education</h4>
        </div>

        <div id="timeline-education">
        </div>
    </div>
</section>

<script>
    function getEducation()
    {
        $.ajax({
            url: "{{ route('get.education') }}",
            success: function (data) {
                let result = data.data
                html = ""
                const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                const level = {
                    'high-school': 'H',
                    'undergraduate': 'B',
                    'graduate': 'M',
                    'postgraduate': 'P',
                    'non-formal': 'N'
                };

                result.forEach(function (value, index) {
                    // charAt(0)
                    let start = new Date(value.start);
                    let end = new Date(value.end);
                    startDate = (monthNames[start.getMonth() + 1]) + " " + start.getFullYear()
                    endDate = (monthNames[end.getMonth() + 1]) + " " + end.getFullYear()

                    html += `
                    <div class="timeline-block">
                        <!-- DOT -->
                        <div class="timeline-dot"><h6>`+ level[value.level] +`</h6></div>
                        <!-- TIMELINE CONTENT -->
                        <div class="card timeline-content">
                            <div class="card-content">
                                <!-- TIMELINE TITLE -->
                                <h6 class="timeline-title">`+ value.major +`</h6>
                                <!-- TIMELINE TITLE INFO -->
                                <div class="timeline-info">
                                    <h6>
                                        <small>`+ value.institution +`</small>
                                    </h6>
                                    <h6>
                                        <small>`+ startDate +` - `+ endDate +`</small>
                                    </h6>
                                </div>
                                <!-- TIMELINE PARAGRAPH -->
                                <p>`+ value.short_description +`</p>
                                <!-- BUTTON TRIGGER MODAL -->
                                <a href="#" class="modal-dot" onclick="getSingleEducation('`+ value.show +`')" data-toggle="modal" data-target="#myModal-1">
                                    <i class="fa fa-ellipsis-h" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    `
                })

                $('#timeline-education').html(html)
            },
            error: function (xhr) {
                console.log(xhr)
            }
        })
    }

    function getSingleEducation(url) {
        $.ajax({
            url: url,
            success: function (data) {
                const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                let result = data.data
                title = ""

                if (result.level == "undergraduate") {
                    title = "Bachelor of "
                } else if (result.level == "graduate") {
                    title = "Master of "
                } else if (result.level == "postgraduate") {
                    title = "Doctor of "
                }

                title += result.major
                let start = new Date(result.start);
                let end = new Date(result.end);
                startDate = (monthNames[start.getMonth()]) + " " + start.getFullYear()
                endDate = (monthNames[end.getMonth()]) + " " + end.getFullYear()

                $('#educationTitle').html(title);
                $('#educationPeriod').html(startDate + " - " + endDate);
                $('#educationDescription').html(result.description);
                $('#educationModal').modal('show');
            },
            error: function (xhr) {
                console.log(xhr)
            }
        })
    }
</script>