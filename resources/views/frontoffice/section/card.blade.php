<!--==========================================
                   V-CARD
===========================================-->
<div id="v-card-holder" class="section">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">

                <!-- V-CARD -->
                <div id="v-card" class="card">

                    <!-- PROFILE PICTURE -->
                    <div id="profile" class="right">
                        <img alt="profile-image" class="img-responsive" src="{{ empty($data->photo) ? asset('frontoffice/assets/images/profile/profile.png') : asset('/storage/basic_information/'.$data->photo) }}">
                        <div class="slant"></div>

                        <!--EMPTY PLUS BUTTON-->
                        <!--<div class="btn-floating btn-large add-btn"><i class="material-icons">add</i></div>-->

                        <!--VIDEO PLAY BUTTON-->
                        @if (!empty($data->video))
                        <div id="button-holder" class="btn-holder">
                            <div id="play-btn" class="btn-floating btn-large btn-play">
                                <i id="icon-play" class="material-icons">play_arrow</i>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!--VIDEO CLOSE BUTTON-->
                    <div id="close-btn" class="btn-floating icon-close">
                        <i class="fa fa-close"></i>
                    </div>

                    <div class="card-content">

                        <!-- NAME & STATUS -->
                        <div class="info-headings">
                            <h4 class="text-uppercase left">{{ empty($data->name) ? "John Doe" : $data->name }}</h4>
                            <h6 class="text-capitalize left">{{ empty($data->position) ? "Software Engineer" : $data->position }}</h6>
                        </div>

                        <!-- CONTACT INFO -->
                        <div class="infos">
                            <ul class="profile-list">
                                @if (!empty($data->email))
                                <li class="clearfix">
                                    <span class="title"><i class="material-icons">email</i></span>
                                    <span class="content">{{ $data->email }}</span>
                                </li>
                                @endif
                                @if (!empty($data->handphone))
                                <li class="clearfix">
                                    <span class="title"><i class="material-icons">phone</i></span>
                                    <span class="content">{{ $data->handphone }}</span>
                                </li>
                                @endif
                                @if (!empty($data->address))
                                <li class="clearfix">
                                    <span class="title"><i class="material-icons">place</i></span>
                                    <span class="content">{{ $data->address }}</span>
                                </li>
                                @endif
                            </ul>
                        </div>

                        <!--LINKS-->
                        <div class="links">
                            @if($data->linkedin)
                            <!-- LINKEDIN-->
                            <a href="{{ $data->linkedin }}" class="social  btn-floating blue darken-3" target="_blank"><i
                                    class="fa fa-linkedin"></i></a>
                            @endif
                            <!-- RSS-->
                            @if($data->git)
                            <a href="{{ $data->git }}" class="social  btn-floating orange darken-3" target="_blank"><i
                                    class="fab fa-gitlab"></i></a>
                            @endif
                        </div>
                    </div>
                    <!--HTML 5 VIDEO-->
                    <video id="html-video" class="video" poster="{{ asset('frontoffice/assets/images/poster/poster.jpg') }}" controls>
                        <!--SERVER HOSTED VIDEO-->
                        <source src="videos/b.webm" type="video/webm">
                        <source src="videos/a.mp4" type="video/mp4">
                    </video>

                </div>
            </div>
        </div>
    </div>
</div>