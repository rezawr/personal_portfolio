<!--==========================================
                  PORTFOLIOS
===========================================-->
<section id="portfolios" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="{{ asset('frontoffice/assets/images/icons/safe.png') }}" alt="demo">Portfolios</h4>
        </div>
        <div id="portfolios-card" class="row">
            <!--OPTIONS-->
            <ul class="nav nav-tabs">
                <!--ALL CATEGORIES-->
                <li class="active waves-effect list-shuffle"><a class="cate" href="#all" data-toggle="tab">WEBSITES</a></li>
                <!--CATEGORY 3-->
                <div class="tab-content">
                    <div id="all">
                    </div>
                </div>
            </ul>
        </div>
    </div>
</section>

<script>
    function getPortfolio()
    {
        $.ajax({
            url: "{{ route('get.portfolio') }}",
            success: function (data) {
                html = ""
                let result = data.data

                result.forEach(function (value, index) {
                    className = (index % 2 == 0) ? "inLeft" : "inRight";
                    html += `
                        <div class="col-md-4 col-sm-6 col-xs-12 grid `+ className +`">
                            <figure class="port-effect-up">
                                <img src="`+ value.file_download +`" class="img-responsive" alt="portfolio-demo"/>
                                <figcaption>
                                    <h2>WEB <span> `+ value.name +`</span></h2>
                                    <p>Showcase Portfolio</p>
                                    <a href="`+ value.show +`">View more</a>
                                </figcaption>
                            </figure>
                        </div>
                    `
                })
                
                $('#all').html(html)
            }
        })
    }
</script>