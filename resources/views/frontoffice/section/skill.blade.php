<!--==========================================
                   SKILLS
===========================================-->
<section id="skills" class="section">
    <div class="container">
        <!-- SECTION TITLE -->
        <div class="section-title">
            <h4 class="text-uppercase text-center"><img src="{{ asset('frontoffice/assets/images/icons/mixer.png') }}" alt="demo">Skills</h4>
        </div>

        <div class="row">
            <div class="col-12">
                <div id="skills-card" class="card">
                    <div class="card-content">
                        <div class="row" id="skill-content"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    function getSkill()
    {
        $.ajax({
            url: "{{ route('get.skill') }}",
            success: function (data) {
                let result = data.data

                html = ""
                let percent, name;
                result.forEach(function (value, index) {
                    name = value.name

                    if (value.level == "excellent") {
                        percent = 100;
                    }
                    if (value.level == "extensive") {
                        percent = 80;
                    }
                    if (value.level == "proficient") {
                        percent = 60;
                    }
                    if (value.level == "experienced") {
                        percent = 40;
                    }
                    if (value.level == "basic") {
                        percent = 20;
                    }

                    html += `
                    <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="skillbar" data-percent="68%">
                            <div class="skillbar-title"><span>`+ name +`</span></div>
                            <div class="skillbar-bar" style="width: `+ percent +`%"></div>
                            <div class="skill-bar-percent">`+ value.level.charAt(0).toUpperCase() + value.level.slice(1) +`</div>
                        </div>
                    </div>
                    `
                })
                $('#skill-content').html(html);
            },
            error: function (xhr) {
                console.log(xhr)
            }
        })
    }
</script>