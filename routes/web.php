<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('frontoffice.index');
// });

// Frontoffice
Route::controller(App\Http\Controllers\Frontoffice\DashboardController::class)->group(function() {
    Route::get('/', 'index')->name('index');
    Route::get('/get_education', 'get_education')->name('get.education');
    Route::get('/get_education/{id}', 'get_show_education')->name('get.education.show');
    Route::get('/get_skill', 'get_skill')->name('get.skill');
    Route::get('/get_experience', 'get_experience')->name('get.experience');
    Route::get('/get_experience/{id}', 'get_show_experience')->name('get.experience.show');
    Route::get('/get_portfolio', 'get_portfolio')->name('get.portfolio');
    Route::get('/get_portfolio/{id}', 'get_show_portfolio')->name('get.portfolio.show');

    Route::post('/post_message', 'post_message')->name('post.message');
});


// Backoffice
Route::controller(App\Http\Controllers\Backoffice\AuthController::class)->group(function () {
    Route::view("backoffice/login", 'backoffice.auth.login')->name("login");
    Route::post("backoffice/login", 'login')->name('post_login');

    Route::get("backoffice/logout", 'logout')->name('logout');
});

Route::group(['middleware' => ['auth', 'auth.session']], function () {
    Route::prefix('backoffice')->name('backoffice.')->group(function() {
        Route::view('/', 'backoffice.dashboard.index')->name('raw_index');
        Route::view('/index', 'backoffice.dashboard.index')->name('index');

        Route::resources([
            'information' => App\Http\Controllers\Backoffice\InformationController::class,
            'education' => App\Http\Controllers\Backoffice\EducationController::class,
            'experience' => App\Http\Controllers\Backoffice\ExperienceController::class,
            'project' => App\Http\Controllers\Backoffice\ProjectController::class,
            'skill' => App\Http\Controllers\Backoffice\SkillController::class,
        ]);
    });
});


