<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InformationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $keys = [
            [
                'name',
                'string',
            ], [
                'position',
                'string',
            ], [
                'email',
                'email',
            ], [
                'handphone',
                'phone_number',
            ], [
                'git',
                'url',
            ], [
                'description',
                'string',
            ], [
                'address',
                'string',
            ], [
                'photo',
                'file',
            ], [
                'video',
                'file',
            ], [
                'background',
                'file',
            ], [
                'cv',
                'file',
            ], [
                'linkedin',
                'url',
            ],
        ];

        foreach ($keys as $key) {
            $val = \App\Models\Information::class::where('key', $key[0])->first();

            if ($val) {
                continue;
            }

            \App\Models\Information::class::create([
                'key' => $key[0],
                'value' => '',
                'type' => $key[1],
            ]);
        }
    }
}
