<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Backoffice\Controller;
use App\Models\Experience;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExperienceController extends Controller
{
    public function __construct()
    {
        $this->template = "backoffice.experience";
        $this->template_data = [
            'title' => "Backoffice > Pengalaman Bekerja",
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->template_data['data'] = Experience::all();

        return parent::index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = 200;
            $raw_desc = html_entity_decode($request->description);
            $desc = $raw_desc;
            $val = "";

            if (strlen($raw_desc) > 155) {
                $desc = substr($raw_desc, 0, 155);
            }

            do {
                $val = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                $val = "{$val}.{$request->file('logo')->extension()}";
            } while (!empty(Experience::where('logo_company', $val)->first()));

            Storage::disk('public')->putFileAs(
                "experience",
                $request->file('logo'),
                $val
            );

            $created = array_merge($request->except('logo'), [
                'short_description' => $desc,
                'start' => "{$request->start}-01",
                'end' => empty($request->end) ? null : "{$request->end}-01",
                'logo_company' => $val,
            ]);

            Experience::create($created);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Experience is saved";
            $this->response_body['data'] = route('backoffice.experience.index');
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function show(Experience $experience)
    {
        try {
            $status = 200;

            $this->response_body['status'] = True;
            $this->response_body['data'] = $experience;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function edit(Experience $experience)
    {
        $this->template_data['experience'] = $experience;
        return view("{$this->template}.edit", $this->template_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Experience $experience)
    {
        try {
            $status = 200;
            $raw_desc = html_entity_decode($request->description);
            $desc = $raw_desc;
            $val = "";

            if (strlen($raw_desc) > 155) {
                $desc = substr($raw_desc, 0, 155);
            }

            $updated = array_merge($request->except('logo'), [
                'short_description' => $desc,
                'start' => "{$request->start}-01",
                'end' => empty($request->end) ? null : "{$request->end}-01",
            ]);

            if (!empty($request->logo)) {
                do {
                    $val = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                    $val = "{$val}.{$request->file('logo')->extension()}";
                } while (!empty(Experience::where('logo_company', $val)->first()));
    
                Storage::disk('public')->putFileAs(
                    "experience",
                    $request->file('logo'),
                    $val
                );

                $updated['logo_company'] = $val;
            }

            $experience->update($updated);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Experience is updated";
            $this->response_body['data'] = route('backoffice.experience.index');
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Experience  $experience
     * @return \Illuminate\Http\Response
     */
    public function destroy(Experience $experience)
    {
        //
    }
}
