<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    protected $response_body = [
        'status' => False,
        'message' => '',
        'data' => ''
    ];

    protected $template = "";
    protected $template_data = [];

    public function _generate_response($response_code)
    {
        return response()->json($this->response_body, $response_code);
    }

    public function index()
    {
        return view("{$this->template}.index", $this->template_data);
    }

    public function create()
    {
        return view("{$this->template}.create", $this->template_data);
    }
}
