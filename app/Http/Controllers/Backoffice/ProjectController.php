<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Backoffice\Controller;
use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    public function __construct()
    {
        $this->template = "backoffice.project";
        $this->template_data = [
            'title' => "Backoffice > Project",
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->template_data['data'] = Project::all();

        return parent::index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = 200;
            $val = "";

            do {
                $val = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                $val = "{$val}.{$request->file('overview')->extension()}";
            } while (!empty(Project::where('overview', $val)->first()));

            Storage::disk('public')->putFileAs(
                "projects",
                $request->file('overview'),
                $val
            );

            $created = array_merge($request->except('overview'), [
                'overview' => $val,
                'type' => json_encode(explode(',', $request->type))
            ]);

            Project::create($created);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Projects is saved";
            $this->response_body['data'] = route('backoffice.project.index');
            $status = 200;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        try {
            $status = 200;

            $this->response_body['status'] = True;
            $this->response_body['data'] = $project;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        $project->type = implode(",", json_decode($project->type));
        $this->template_data['project'] = $project;
        return view("{$this->template}.edit", $this->template_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        try {
            $status = 200;
            $updated = array_merge($request->except('overview'), [
                'type' => json_encode(explode(',', $request->type))
            ]);

            if (!empty($request->overview)) {
                $val = "";
    
                do {
                    $val = substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
                    $val = "{$val}.{$request->file('overview')->extension()}";
                } while (!empty(Project::where('overview', $val)->first()));
    
                Storage::disk('public')->putFileAs(
                    "projects",
                    $request->file('overview'),
                    $val
                );
                $updated["overview"] = $val;
            }

            $project->update($updated);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Projects is updated";
            $this->response_body['data'] = route('backoffice.project.index');
            $status = 200;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
