<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Backoffice\Controller;
use App\Models\Skill;
use Illuminate\Http\Request;

class SkillController extends Controller
{
    public function __construct()
    {
        $this->template = "backoffice.skill";
        $this->template_data = [
            'title' => "Backoffice > Skills",
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->template_data['data'] = Skill::all();

        return parent::index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = 200;

            Skill::create($request->all());

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Skill is saved";
            $this->response_body['data'] = route('backoffice.skill.index');
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function show(Skill $skill)
    {
        try {
            $status = 200;

            $this->response_body['status'] = True;
            $this->response_body['data'] = $skill;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function edit(Skill $skill)
    {
        $this->template_data['skill'] = $skill;
        return view("{$this->template}.edit", $this->template_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Skill $skill)
    {
        try {
            $status = 200;

            $skill->update($request->all());
            $this->response_body['status'] = True;
            $this->response_body['message'] = "Skill Updated";
            $this->response_body['data'] = route('backoffice.skill.index');
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Skill  $skill
     * @return \Illuminate\Http\Response
     */
    public function destroy(Skill $skill)
    {
        //
    }
}
