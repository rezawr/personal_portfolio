<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Backoffice\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        try {
            $validated = Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required'
            ]);
    
            if ($validated->fails()) {
                $this->response_body['message'] = $validated->errors()->all();
                return $this->_generate_response(422);
            }

            if (Auth::guard('web')->attempt([
                'email' => $request->email,
                'password' => $request->password
            ])) {
                Auth::guard('web')->logoutOtherDevices($request->password);
                $request->session()->regenerate();
                $this->response_body['status'] = "True";
                $this->response_body['message'] = "Login Success";
                $this->response_body['data'] = route('backoffice.index');
                $status = 200;
    
            } else {
                $this->response_body['message'] = "Email or Password Mismatch";
                $status = 500;
            }
        } catch (\Throwable $t) {
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function logout()
    {
        Auth::guard('web')->logout();

        return redirect(route('login'));
    }
}
