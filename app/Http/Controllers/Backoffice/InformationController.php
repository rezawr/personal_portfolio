<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Backoffice\Controller;
use App\Models\Information;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class InformationController extends Controller
{
    public function __construct()
    {
        $this->template = "backoffice.information";
    }

    public function index()
    {
        $this->template_data = [
            'title' => "Backoffice > Informasi",
            'data' => Information::all(),
        ];

        return parent::index();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function show(Information $information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function edit(Information $information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Information $information)
    {
        try {
            $status = 200;

            if ($request->hasFile('value')){
                $val = "{$information->key}.{$request->file('value')->extension()}";

                Storage::disk('public')->putFileAs(
                    "basic_information",
                    $request->file('value'),
                    $val
                );
            } else {
                $val = $request->value;
            }
            $information->update([
                'value' => $val
            ]);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Information is saved";
            $status = 200;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Information  $information
     * @return \Illuminate\Http\Response
     */
    public function destroy(Information $information)
    {
        //
    }
}
