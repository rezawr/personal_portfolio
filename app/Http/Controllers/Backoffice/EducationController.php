<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Backoffice\Controller;
use App\Models\Education;
use Illuminate\Http\Request;

class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->template = "backoffice.education";
        $this->template_data = [
            'title' => "Backoffice > Pendidikan",
        ];
    }

    public function index()
    {
        $this->template_data['data'] = Education::all();

        return parent::index();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $status = 200;
            $raw_desc = html_entity_decode($request->description);
            $desc = $raw_desc;

            if (strlen($raw_desc) > 155) {
                $desc = substr($raw_desc, 0, 155);
            }

            $created = array_merge($request->all(), [
                'short_description' => $desc,
                'start' => "{$request->start}-01",
                'end' => "{$request->end}-01",
            ]);

            Education::create($created);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Education is saved";
            $this->response_body['data'] = route('backoffice.education.index');
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function show(Education $education)
    {
        try {
            $status = 200;

            $this->response_body['status'] = True;
            $this->response_body['data'] = $education;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function edit(Education $education)
    {
        $this->template_data['education'] = $education;
        return view("{$this->template}.edit", $this->template_data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Education $education)
    {
        try {
            $status = 200;
            $raw_desc = html_entity_decode($request->description);
            $desc = $raw_desc;

            if (strlen($raw_desc) > 155) {
                $desc = substr($raw_desc, 0, 155);
            }

            $update = array_merge($request->all(), [
                'short_description' => $desc,
                'start' => "{$request->start}-01",
                'end' => "{$request->end}-01",
            ]);

            $education->update($update);

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Education is updated";
            $this->response_body['data'] = route('backoffice.education.index');
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function destroy(Education $education)
    {
        //
    }
}
