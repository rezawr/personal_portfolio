<?php

namespace App\Http\Controllers\Frontoffice;

use App\Http\Controllers\Frontoffice\Controller;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $informations = \App\Models\Information::class::all();

        $data = new \StdClass;
        foreach ($informations as $information) {
            $data->{$information->key} = $information->value;
        }

        return view('frontoffice.index', [
            'data' => $data
        ]);
    }

    public function get_education(Request $request)
    {
        try {
            $status = 200;
            $result = \App\Models\Education::class::orderBy('end', 'ASC')->get();

            foreach ($result as $index => $row) {
                $result[$index]->show = route('get.education.show', $row->id);
            }

            $this->response_body['status'] = True;
            $this->response_body['data'] = $result;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function get_show_education(Request $request)
    {
        try {
            $status = 200;

            $data = \App\Models\Education::class::findOrFail($request->id);
            $this->response_body['status'] = True;
            $this->response_body['data'] = $data;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function get_skill()
    {
        try {
            $status = 200;
            $order = [
                'excellent' => [],
                'extensive' => [],
                'proficient' => [],
                'experienced' => [],
                'basic' => [],
            ];

            $result = \App\Models\Skill::class::all();

            foreach ($result as $index => $row) {
                $order[$row->level][] = $row;
            }

            $result = array_merge($order['excellent'], $order['extensive'], $order['proficient'], $order['experienced'], $order['basic']);

            $this->response_body['status'] = True;
            $this->response_body['data'] = $result;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function get_experience()
    {
        try {
            $status = 200;
            $result = \App\Models\Experience::class::orderBy('current', 'ASC')->orderBy('end', 'ASC')->get();

            foreach ($result as $index => $row) {
                $result[$index]->show = route('get.experience.show', $row->id);
            }

            $this->response_body['status'] = True;
            $this->response_body['data'] = $result;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function get_show_experience(Request $request)
    {
        try {
            $status = 200;

            $data = \App\Models\Experience::class::findOrFail($request->id);
            $this->response_body['status'] = True;
            $this->response_body['data'] = $data;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function get_portfolio()
    {
        try {
            $status = 200;
            $result = \App\Models\Project::class::orderBy('delivered', 'DESC')->get();

            foreach ($result as $index => $row) {
                $result[$index]->show = route('get.portfolio.show', $row->id);
            }

            $this->response_body['status'] = True;
            $this->response_body['data'] = $result;
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }

    public function get_show_portfolio(Request $request)
    {
        $informations = \App\Models\Information::class::all();

        $data = new \StdClass;
        foreach ($informations as $information) {
            $data->{$information->key} = $information->value;
        }

        $portfolio = \App\Models\Project::class::findOrFail($request->id);
        $portfolio->type = implode(",", json_decode($portfolio->type));
        
        return view('frontoffice.portfolio', [
            'portfolio' => $portfolio,
            'data' => $data
        ]);
    }

    public function post_message(Request $request)
    {
        try {
            $status = 200;

            \App\Models\Message::create($request->all());

            $this->response_body['status'] = True;
            $this->response_body['message'] = "Message is sent";
        } catch (\Throwable $t) {
            $this->response_body['status'] = False;
            $this->response_body['message'] = $t->getMessage();
            $status = 500;
        }

        return $this->_generate_response($status);
    }
}
