<?php

namespace App\Http\Controllers\Frontoffice;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{
    protected $response_body = [
        'status' => False,
        'message' => '',
        'data' => ''
    ];

    public function _generate_response($response_code)
    {
        return response()->json($this->response_body, $response_code);
    }
}
