<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Model;

class Education extends Model
{
    use HasFactory;

    protected $base_url = 'backoffice.education';

    protected $fillable = [
        'level',
        'major',
        'institution',
        'start',
        'end',
        'short_description',
        'description',
    ];

    protected $appends = ['dependent_url'];
}
