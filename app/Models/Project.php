<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Model;

class Project extends Model
{
    use HasFactory;

    protected $base_url = 'backoffice.project';

    protected $fillable = [
        'name',
        'overview',
        'client',
        'delivered',
        'link',
        'type',
        'details',
    ];

    protected $appends = ['dependent_url', 'file_download'];

    public function getFileDownloadAttribute()
    {
        return asset("/storage/projects/{$this->attributes['overview']}");
    }
}
