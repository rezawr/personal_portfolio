<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    use HasFactory;
    protected $base_url;

    public function getDependentUrlAttribute()
    {
        $url = new \StdClass;
        $url->edit = route("{$this->base_url}.edit", $this->attributes['id']);
        $url->update = route("{$this->base_url}.update", $this->attributes['id']);
        $url->show = route("{$this->base_url}.show", $this->attributes['id']);
        $url->delete = route("{$this->base_url}.destroy", $this->attributes['id']);

        return $url;
    }
}
