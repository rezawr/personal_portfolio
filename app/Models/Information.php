<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Model;

class Information extends Model
{
    use HasFactory;

    protected $base_url = 'backoffice.information';

    protected $fillable = [
        'key',
        'type',
        'value',
    ];

    protected $appends = ['dependent_url'];
}
