<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Model;

class Experience extends Model
{
    use HasFactory;

    protected $base_url = 'backoffice.experience';

    protected $fillable = [
        'position',
        'company',
        'logo_company',
        'start',
        'end',
        'current',
        'description',
        'short_description',
    ];

    protected $appends = ['dependent_url', 'file_download'];

    public function getFileDownloadAttribute()
    {
        return asset("/storage/experience/{$this->attributes['logo_company']}");
    }
}
