<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Model;

class Skill extends Model
{
    use HasFactory;

    protected $base_url = "backoffice.skill";

    protected $fillable = [
        'name',
        'level',
    ];

    protected $appends = ['dependent_url'];
}
